# Swagger\Client\SubscriptionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSubscription**](SubscriptionsApi.md#createSubscription) | **POST** /organizations/{organizationUuid}/subscriptions | Creates a single subscription
[**getSubscriptions**](SubscriptionsApi.md#getSubscriptions) | **GET** /organizations/{organizationUuid}/subscriptions | Gets all subscriptions for an organization
[**removeSubscription**](SubscriptionsApi.md#removeSubscription) | **DELETE** /organizations/{organizationUuid}/subscriptions/uuid/{uuid} | Deletes a single subscription
[**updateSubscription**](SubscriptionsApi.md#updateSubscription) | **PUT** /organizations/{organizationUuid}/subscriptions/{subscriptionUuid} | Updates an existing subscription


# **createSubscription**
> \Swagger\Client\Model\SubscriptionResponse createSubscription($organization_uuid, $body)

Creates a single subscription

Creates a webhook subscription for one callback url

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\CreateSubscription(); // \Swagger\Client\Model\CreateSubscription | 

try {
    $result = $apiInstance->createSubscription($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionsApi->createSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\CreateSubscription**](../Model/CreateSubscription.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSubscriptions**
> \Swagger\Client\Model\SubscriptionResponse getSubscriptions($organization_uuid)

Gets all subscriptions for an organization



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 

try {
    $result = $apiInstance->getSubscriptions($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionsApi->getSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\SubscriptionResponse**](../Model/SubscriptionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeSubscription**
> removeSubscription($organization_uuid, $uuid)

Deletes a single subscription



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$uuid = "uuid_example"; // string | 

try {
    $apiInstance->removeSubscription($organization_uuid, $uuid);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionsApi->removeSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **uuid** | [**string**](../Model/.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSubscription**
> updateSubscription($organization_uuid, $subscription_uuid, $body)

Updates an existing subscription



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SubscriptionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$subscription_uuid = "subscription_uuid_example"; // string | 
$body = new \Swagger\Client\Model\UpdateSubscription(); // \Swagger\Client\Model\UpdateSubscription | 

try {
    $apiInstance->updateSubscription($organization_uuid, $subscription_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionsApi->updateSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **subscription_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\UpdateSubscription**](../Model/UpdateSubscription.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

