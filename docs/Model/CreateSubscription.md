# CreateSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**transport_name** | **string** |  | 
**event_names** | **string[]** | Event names to subscribe to. Leave empty if you want to subscribe to all | [optional] 
**destination** | **string** |  | 
**contact_email** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


