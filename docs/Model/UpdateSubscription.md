# UpdateSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_names** | **string[]** | Event names to subscribe to. Leave empty if you want to subscribe to all | [optional] 
**destination** | **string** |  | [optional] 
**contact_email** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


