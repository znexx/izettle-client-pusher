# SubscriptionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** | Identifies the subscription | 
**transport_name** | **string** | Type of transport | 
**event_names** | **string[]** | EventNames | 
**updated** | **int** | Timestamp when last updated. | 
**destination** | **string** | Where to push events. Usually an URL | 
**contact_email** | **string** | Contact email for the subscription, automatic email will be sent if endpoint fails. | [optional] 
**status** | **string** | Status of subscription | 
**signing_key** | **string** | The HMACSHA256 key used to sign the webhook payload | 
**client_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


